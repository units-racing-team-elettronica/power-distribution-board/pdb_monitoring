#include "SAMC21_BSP.h"
#include "CommSystem.h"
#include "PdbPowerMonitor.h"

int main(void)
{
	SYSTEM_InitializeClocks(SYSTEM_OSCILLATOR_EXTOSC);
	SYSTEM_InitalizeSysTick(SYSTEM_SYSTICK_FREQ);

	CommSystem_Initialize();
	PdbPwrMon_Initialize();
	
	while(1) {
		CommSystem_Task();
		PdbPwrMon_Task();
	}
}

