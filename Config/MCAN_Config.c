/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MCAN_Driver.h"
#include "MCAN_Config.h"

#if(MCAN0_ENABLED)

MCAN_CONFIGURATION MCAN0_Config = {
    .RxStandardFilters = MCAN0_RX_STANDARD_FILTERS,
    .RxExtendedFilters = MCAN0_RX_EXTENDED_FILTERS,
    .RxDedicatedBuffers = MCAN0_RX_DEDICATED_BUFFERS,
	.RxFifo0Length = MCAN0_RX_FIFO0_LENGHT,
	.RxFifo1Length = MCAN0_RX_FIFO1_LENGHT,
	.TxDedicatedBuffers = MCAN0_TX_DEDICATED_BUFFERS,
	.TxFifoLenght = MCAN0_TX_FIFO_LENGHT,
	.TxEventFifoLenght = MCAN0_TX_EVENT_FIFO_LENGHT,
	.BitTimings = MCAN0_CONFIG_BITTIMINGS,
	.GlobalFilter = MCAN0_GLOBAL_FILTER_CONFIG,
	.ExtendedIdAndMask = MCAN0_GLOBAL_EXTIDANDMASK,
	.StdInitialFiltersNum = MCAN0_STDFILTERS_PRELOAD,
#if(MCAN0_STDFILTERS_PRELOAD)
	.StdInitialFilters = _mcan0_init_stdfilters,
#else
	.StdInitialFilters = NULL,
#endif
	.ExtInitialFiltersNum = MCAN0_EXTFILTERS_PRELOAD,
#if(MCAN0_EXTFILTERS_PRELOAD)
	.ExtInitialFilters = _mcan0_init_extfilters,
#else
	.ExtInitialFilters = NULL,
#endif
};

#endif

#if(MCAN1_ENABLED)

MCAN_CONFIGURATION MCAN1_Config = {
    .RxStandardFilters = MCAN1_RX_STANDARD_FILTERS,
    .RxExtendedFilters = MCAN1_RX_EXTENDED_FILTERS,
    .RxDedicatedBuffers = MCAN1_RX_DEDICATED_BUFFERS,
	.RxFifo0Length = MCAN1_RX_FIFO0_LENGHT,
	.RxFifo1Length = MCAN1_RX_FIFO1_LENGHT,
	.TxDedicatedBuffers = MCAN1_TX_DEDICATED_BUFFERS,
	.TxFifoLenght = MCAN1_TX_FIFO_LENGHT,
	.TxEventFifoLenght = MCAN1_TX_EVENT_FIFO_LENGHT,
	.BitTimings = MCAN1_CONFIG_BITTIMINGS,
	.GlobalFilter = MCAN1_GLOBAL_FILTER_CONFIG,
	.ExtendedIdAndMask = MCAN1_GLOBAL_EXTIDANDMASK,
	.StdInitialFiltersNum = MCAN1_STDFILTERS_PRELOAD,
#if(MCAN1_STDFILTERS_PRELOAD)
	.StdInitialFilters = _mcan1_init_stdfilters,
#else
	.StdInitialFilters = NULL,
#endif
	.ExtInitialFiltersNum = MCAN1_EXTFILTERS_PRELOAD,
#if(MCAN1_EXTFILTERS_PRELOAD)
	.ExtInitialFilters = _mcan1_init_extfilters,
#else
	.ExtInitialFilters = NULL,
#endif
};

#endif

#if(MCAN0_ENABLED)

WORD _mcan0_init_stdfilters[MCAN0_STDFILTERS_PRELOAD] = {};
WORD _mcan0_init_extfilters[MCAN0_EXTFILTERS_PRELOAD * WORDS_PER_EXTENDED_FILTER] = {};

#endif

#if(MCAN1_ENABLED)

WORD _mcan1_init_stdfilters[MCAN1_STDFILTERS_PRELOAD] = {};
WORD _mcan1_init_extfilters[MCAN1_EXTFILTERS_PRELOAD * WORDS_PER_EXTENDED_FILTER] = {};

#endif