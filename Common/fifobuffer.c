/*
Copyright (c) 2017-2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "fifobuffer.h"

void CreateStaticFifoBuffer(FIFO_BUFFER* handle, CHAR* memory, WORD size)
{
    handle->Buffer = memory;
    handle->Head = handle->Tail = memory;
    handle->Lenght = 0;
    handle->MaxLenght = size;
	handle->LockHandle = NULL;
	handle->SyncOptions = 0;
}

void FifoBuffer_SetLockFunc(FIFO_BUFFER* handle, DWORD (*lockfunc)(HANDLE), void (*unlockfunc)(HANDLE, DWORD), HANDLE lockhandle, DWORD syncoptions)
{
	handle->LockHandle = lockhandle;
	handle->LockFunction = lockfunc;
	handle->UnlockFunction = unlockfunc;
	handle->SyncOptions = syncoptions;
}

void FifoBuffer_Flush(FIFO_BUFFER* handle)
{
	DWORD lock = 0;
	if(handle->LockHandle)
		lock = handle->LockFunction(handle->LockHandle);
	if(handle->SyncOptions & FIFO_SYNCHRONIZE_WRITES)
		lock = LOCK_WRITESTOP;
	handle->Head = handle->Buffer;
	handle->Tail = handle->Buffer;
	handle->Lenght = 0;
	if(handle->LockHandle)
		handle->UnlockFunction(handle->LockHandle, lock);
}

BOOLEAN FifoBuffer_FlushBytes(FIFO_BUFFER* handle, WORD len)
{
	if(len > handle->Lenght)
		return FALSE;
	DWORD lock = 0;
	if(handle->LockHandle)
		lock = handle->LockFunction(handle->LockHandle);
	if(handle->Head - handle->Buffer + len >= handle->MaxLenght)
		handle->Head = handle->Buffer + (len - (handle->MaxLenght - (handle->Head - handle->Buffer)));
	else
		handle->Head += len;
	handle->Lenght -= len;
	if(handle->LockHandle)
		handle->UnlockFunction(handle->LockHandle, lock);
	return TRUE;
}


BOOLEAN FifoBuffer_WriteByte(FIFO_BUFFER* handle, CHAR data)
{
	if(handle->Lenght == handle->MaxLenght)
		return FALSE;
	DWORD lock = 0;
	if(handle->LockHandle)
		lock = handle->LockFunction(handle->LockHandle);
	if(handle->SyncOptions & FIFO_SYNCHRONIZE_WRITES)
		lock = LOCK_WRITESYNCHRONIZE;
	*handle->Tail = data;
	handle->Lenght++;
	if(++handle->Tail - handle->Buffer == handle->MaxLenght)
		handle->Tail = handle->Buffer;
	if(handle->LockHandle)
		handle->UnlockFunction(handle->LockHandle, lock);
	return TRUE;
}

BOOLEAN FifoBuffer_WriteByteFromISR(FIFO_BUFFER* handle, CHAR data)
{
	if(handle->Lenght == handle->MaxLenght)
		return FALSE;
	*handle->Tail = data;
	handle->Lenght++;
	if(++handle->Tail - handle->Buffer == handle->MaxLenght)
		handle->Tail = handle->Buffer;
	return TRUE;
}


BOOLEAN FifoBuffer_WriteBytes(FIFO_BUFFER* handle, CHAR *data, WORD len)
{
	if(handle->Lenght + len == handle->MaxLenght)
		return FALSE;
	DWORD lock = 0;
	if(handle->LockHandle)
		lock = handle->LockFunction(handle->LockHandle);
	if(handle->SyncOptions & FIFO_SYNCHRONIZE_WRITES)
		lock = LOCK_WRITESYNCHRONIZE;
	handle->Lenght += len;
	while(len-- > 0){
		*handle->Tail = *data++;
		if(++handle->Tail - handle->Buffer == handle->MaxLenght)
			handle->Tail = handle->Buffer;
	}
	if(handle->LockHandle)
		handle->UnlockFunction(handle->LockHandle, lock);
	return TRUE;
}

BOOLEAN FifoBuffer_WriteString(FIFO_BUFFER* handle, CHAR *data)
{
	DWORD lock = 0;
	if(handle->LockHandle)
		lock = handle->LockFunction(handle->LockHandle);
	if(handle->SyncOptions & FIFO_SYNCHRONIZE_WRITES)
		lock = LOCK_WRITESYNCHRONIZE;
	INT32 max_len = handle->MaxLenght - handle->Lenght;
	while(*data != '\0') {
		if(max_len == 0) {
			if(handle->LockHandle)
				handle->UnlockFunction(handle->LockHandle, lock);
			return FALSE;
		}
		*handle->Tail = *data++;
		handle->Lenght++;
		if(++handle->Tail - handle->Buffer == handle->MaxLenght)
			handle->Tail = handle->Buffer;
		max_len--;
	}
	if(handle->LockHandle)
		handle->UnlockFunction(handle->LockHandle, lock);
	return TRUE;
}

BOOLEAN FifoBuffer_ReadByte(FIFO_BUFFER* handle, CHAR *data)
{
	if(handle->Lenght == 0)
		return FALSE;
	DWORD lock = 0;
	if(handle->LockHandle)
		lock = handle->LockFunction(handle->LockHandle);
	*data = *handle->Head;
	handle->Lenght--;
	if(++handle->Head - handle->Buffer == handle->MaxLenght)
		handle->Head = handle->Buffer;
	if(handle->LockHandle)
		handle->UnlockFunction(handle->LockHandle, lock);
	return TRUE;
}

BOOLEAN FifoBuffer_ReadByteFromISR(FIFO_BUFFER* handle, CHAR *data)
{
	if(handle->Lenght == 0)
		return FALSE;
	*data = *handle->Head;
	handle->Lenght--;
	if(++handle->Head - handle->Buffer == handle->MaxLenght)
		handle->Head = handle->Buffer;
	return TRUE;
}

BOOLEAN FifoBuffer_ReadBytes(FIFO_BUFFER* handle, CHAR *data, WORD len)
{
	if(handle->Lenght < len)
		return FALSE;
	DWORD lock = 0;
	if(handle->LockHandle)
		lock = handle->LockFunction(handle->LockHandle);
	handle->Lenght -= len;
	while(len-- > 0){
		*data = *handle->Head;
		if(++handle->Head - handle->Buffer == handle->MaxLenght)
			handle->Head = handle->Buffer;
	}
	if(handle->LockHandle)
		handle->UnlockFunction(handle->LockHandle, lock);
	return TRUE;
}

INT32 FifoBuffer_PeekBytesUntil(FIFO_BUFFER* handle, CHAR match, CHAR *data, WORD max_len) 
{
	DWORD chars_read = 0;
	CHAR* newhead = (CHAR*)handle->Head;
	while(1) {
		*data++ = *newhead;
		chars_read++;
		if(*newhead == match)
			return chars_read;
		if(chars_read == handle->Lenght)
			return -1; //NOT FOUND (READ THE ENTIRE BUFFER)
		if(chars_read == max_len)
			return -2; //DESTINATION BUFFER SIZE INSUFFICIENT
		if(++newhead - handle->Buffer == handle->MaxLenght)
			newhead = handle->Buffer;
	}
}

WORD FifoBuffer_GetLenght(FIFO_BUFFER* handle)
{
	return handle->Lenght;
}

WORD FifoBuffer_GetSize(FIFO_BUFFER* handle)
{
	return handle->MaxLenght;
}



