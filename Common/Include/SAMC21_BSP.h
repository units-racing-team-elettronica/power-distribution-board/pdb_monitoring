#ifndef _SAMC21_BSP_GLOBAL_H
#define _SAMC21_BSP_GLOBAL_H

#include "samc21.h"
#include "common.h"
#include "General_Config.h"
#include "ADC_Driver.h"
#include "GPIO_Driver.h"
#if(MCAN0_ENABLED || MCAN1_ENABLED)
#include "MCAN_Driver.h"
#endif
#include "SERCOM_Driver.h"
#include "SYSTEM_Driver.h"

#endif