#include "SAMC21_BSP.h"
#include "CommSystem.h"
#include "PdbPowerMonitor.h"
#include "CANID_definitions.h"

#define PARAM_STATUS_MESSAGE_INTERVAL 100
#define PARAM_DATA_MESSAGE_INTERVAL 20

BOOLEAN ECUOnline = FALSE;

DWORD lastStatusMessage;

void CommSystem_SendStatus();
void CommSystem_ComposeMessage(MCAN_MESSAGE *msg, WORD Id, BYTE length,  WORD *val);

void CommSystem_Initialize(){
    GPIO_SetPinMode(GPIO_PORTB, 22, GPIO_PINMODE_OUTPUT);
	GPIO_SetPinMux(GPIO_PORTB, 22, GPIO_PINMUX_CANSWD);
	GPIO_SetPinMode(GPIO_PORTB, 23, GPIO_PINMODE_INPUT);
	GPIO_SetPinMux(GPIO_PORTB, 23, GPIO_PINMUX_CANSWD);	
	MCAN_Initialize(&MCAN0);
	MCAN_ChangeMode(&MCAN0, MCAN_MODE_ENABLED);
}

void CommSystem_Task(){
    MCAN_MESSAGE rx_msg;
    BOOLEAN res;
    res = MCAN_ReadFIFOMessage(&MCAN0, 0, &rx_msg);
    if(res){
        if(rx_msg.Id == ECU_KEEPALIVE_CANID){
            if(!ECUOnline)
                ECUOnline = TRUE;
        }
        else if(rx_msg.Id == PDB_PWRMON_eFUSE_CONTROL_CANID){
            PdbPwrMon_eFuseControl(rx_msg.Data[0]);
        }
    }

    if(SYSTEM_GetSysTickCount() - lastStatusMessage >= PARAM_STATUS_MESSAGE_INTERVAL){
        lastStatusMessage = SYSTEM_GetSysTickCount();
        if(ECUOnline){
            CommSystem_SendStatus();
        }
    }
}

void CommSystem_SendData(){
    if(ECUOnline){
        PDB_PWRMON_DATA *data = PdbPwrMon_GetData();
        MCAN_MESSAGE msg = {0};
        CommSystem_ComposeMessage(&msg, PDB_PWRMON_IMON_12V_CANID, 6, data->Imon12V);
        MCAN_TransmitMessageFIFO(&MCAN0, &msg);
        MCAN_MESSAGE msg1 = {0};
        CommSystem_ComposeMessage(&msg1, PDB_PWRMON_IMON_INV_CANID, 4, data->ImonInv);
        MCAN_TransmitMessageFIFO(&MCAN0, &msg1);
        MCAN_MESSAGE msg2 = {0};
        CommSystem_ComposeMessage(&msg2, PDB_PWRMON_VMON_CANID, 6, data->Vmon);
        MCAN_TransmitMessageFIFO(&MCAN0, &msg2);
    }
}

void CommSystem_SendStatus(){
    PDB_PWRMON_DATA *data = PdbPwrMon_GetData();
    MCAN_MESSAGE msg = {0};
    msg.Id = PDB_PWRMON_STATUS_CANID;
    msg.Data[0] = data->eFuseStatus & 0xFF;
    msg.Data[0] = data->ExtStatus & 0xFF;
    msg.Lenght = 2;
    MCAN_TransmitMessageFIFO(&MCAN0, &msg);
}

void CommSystem_SendFault(BYTE faultData){
    MCAN_MESSAGE msg = {0};
    msg.Id = PDB_PWRMON_eFUSE_FAULT_CANID;
    msg.Data[0] = faultData;
    msg.Lenght = 1; 
    MCAN_TransmitMessageFIFO(&MCAN0, &msg);
}



void CommSystem_ComposeMessage(MCAN_MESSAGE *msg, WORD Id, BYTE length,  WORD *val){
	msg->Id = Id;
	msg->Lenght = length;
	for( BYTE i = 0; i < length; i++){
		msg->Data[i]= i%2 == 0 ?  (BYTE)(*(val+i/2) & 0xFF) : (BYTE)(*(val+i/2) >> 8);
	}
}