#ifndef _COMM_SYSTEM_
#define _COMM_SYSTEM_

#include "basetypes.h"


void CommSystem_Initialize();
void CommSystem_Task();
void CommSystem_SendData();
void CommSystem_SendFault(BYTE fault);

#endif