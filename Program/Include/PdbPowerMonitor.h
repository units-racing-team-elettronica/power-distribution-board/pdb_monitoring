#ifndef _PDB_POWER_MONITOR_
#define _PDB_POWER_MONITOR_

#include "basetypes.h"

typedef struct pdb_pwrmon_data{
    WORD Imon12V[3];// [0]:SAFAM  [1]:ECUPC  [2]:DATEL
    WORD ImonInv[2];// [0]:INV_F  [1]:INV_A
    WORD Vmon[3];// [0]:12V  [1]:LVS  [2]:EXT
    WORD Tmon12V;
    BYTE eFuseStatus;
    BYTE ExtStatus;
}PDB_PWRMON_DATA;



#define PDB_EXT_NOT_AV 0
#define PDB_EXT_AV 1
#define PDB_EXT_AV_SEL 2

#define PDB_eFUSE_CONTROL_ENABLE 1
#define PDB_eFUSE_CONTROL_DISABLE 0

#define _BV(x) (1 << (x))

void PdbPwrMon_Initialize();
void PdbPwrMon_Task();
void PdbPwrMon_eFuseControl(BYTE eFuseControl);
PDB_PWRMON_DATA* PdbPwrMon_GetData();

#endif