#include "SAMC21_BSP.h"
#include "CommSystem.h"
#include "PdbPowerMonitor.h"

#define PDB_12V_VSCALE(x)	(x/2*11)
#define PDB_LVS_VSCALE(x)	(x/2*23)
#define PDB_ISCALE(x)	(x*200/279)//correggere
#define PDB_TSCALE(x)    1//TODO

#define PDB_PIN_LVS_VMON 2
#define PDB_PIN_12V_VMON 5
#define PDB_PIN_EXT_VMON 8 //PORTB

#define PDB_PIN_12V_TMON 6

#define PDB_PIN_SAFAM_IMON 7
#define PDB_PIN_ECUPC_IMON 8
#define PDB_PIN_DATEL_IMON 9
#define PDB_PIN_INV_F_IMON 3
#define PDB_PIN_INV_A_IMON 4

#define PDB_PIN_SAFAM_PEN 12
#define PDB_PIN_ECUPC_PEN 13
#define PDB_PIN_DATEL_PEN 18
#define PDB_PIN_INV_F_PEN 10
#define PDB_PIN_INV_A_PEN 11

#define PDB_PIN_SAFAM_nFAULT 21
#define PDB_PIN_ECUPC_nFAULT 25
#define PDB_PIN_DATEL_nFAULT 27
#define PDB_PIN_INV_F_nFAULT 19
#define PDB_PIN_INV_A_nFAULT 20

#define PDB_PIN_EXT_SEL 9 //PORTB
#define PDB_PIN_EXT_LED 10 //PORTB
//#define PDB_PIN_LED2 11 //PORTB


#define PDB_eFUSE_MASK 0x31// bits: |-|-|-|inv A|inv F|SAFAM|ECUPC|DATEL|
#define PDB_eFUSE_STARTVALUE 0x07//TODO: da schematico

#define PDB_eFUSE_SAFAM_MASK 0x10
#define PDB_eFUSE_ECUPC_MASK 0x08
#define PDB_eFUSE_DATEL_MASK 0x04
#define PDB_eFUSE_INV_F_MASK 0x02
#define PDB_eFUSE_INV_A_MASK 0x01

#define PARAM_eFUSE_BACKOFF 5000
#define PARAM_eFUSE_FAULT_RETRY 3
#define PARAM_ACQ_TIME 20
#define PARAM_EXT_AVAILABLE_MILLIVOLTS 18000 
#define PARAM_EXT_SEL_BLINK_PERIOD 500

void PdbPwrMon_ADC();
void PdbPwrMon_FaultControl();
void PdbPwrMon_eFuseControl(BYTE eFuseControl);

PDB_PWRMON_DATA PdbPwrMonData;

DWORD lastAcqTime = 0;

volatile DWORD safamFaultTime = 0;
volatile DWORD ecupcFaultTime = 0;
volatile DWORD datelFaultTime = 0;
volatile DWORD invfFaultTime = 0;
volatile DWORD invaFaultTime = 0;

volatile INT8 safamFaults = 0;
volatile INT8 ecupcFaults = 0;
volatile INT8 datelFaults = 0;
volatile INT8 invfFaults = 0;
volatile INT8 invaFaults = 0;

DWORD lastExtLedBlinkTime = 0;

void PdbPwrMon_Initialize(){
    //ADC
    //TODO: controllare port-pin con nuovo schematico
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_LVS_VMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_LVS_VMON, GPIO_PINMUX_ANALOG);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_12V_VMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_12V_VMON, GPIO_PINMUX_ANALOG);
    GPIO_SetPinMode(GPIO_PORTB, PDB_PIN_EXT_VMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTB, PDB_PIN_EXT_VMON, GPIO_PINMUX_ANALOG);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_12V_TMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_12V_TMON, GPIO_PINMUX_ANALOG);

    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_INV_F_IMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_INV_F_IMON, GPIO_PINMUX_ANALOG);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_INV_A_IMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_INV_A_IMON, GPIO_PINMUX_ANALOG);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_SAFAM_IMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_SAFAM_IMON, GPIO_PINMUX_ANALOG);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_ECUPC_IMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_ECUPC_IMON, GPIO_PINMUX_ANALOG);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_DATEL_IMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_DATEL_IMON, GPIO_PINMUX_ANALOG);
   
    

    //OUTPUT
    //TODO: controllare port-pin con nuovo schematico
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_SAFAM_PEN, GPIO_PINMODE_OUTPUT);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_ECUPC_PEN, GPIO_PINMODE_OUTPUT);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_DATEL_PEN, GPIO_PINMODE_OUTPUT);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_INV_A_PEN, GPIO_PINMODE_OUTPUT);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_INV_F_PEN, GPIO_PINMODE_OUTPUT);
    GPIO_SetPinMode(GPIO_PORTB, PDB_PIN_EXT_LED, GPIO_PINMODE_OUTPUT);
    //GPIO_SetPinMode(GPIO_PORTB, PDB_PIN_LED2, GPIO_PINMODE_OUTPUT);

    PdbPwrMon_eFuseControl(PDB_eFUSE_STARTVALUE);

    GPIO_SetPinMode(GPIO_PORTB, PDB_PIN_EXT_SEL, GPIO_PINMODE_INPUT);

    //EXTINT
	//TODO: controllare port-pin con nuovo schematico
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_SAFAM_nFAULT, GPIO_PINMODE_INPUT);	
    GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_SAFAM_nFAULT, GPIO_PINMUX_EXTINT);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_ECUPC_nFAULT, GPIO_PINMODE_INPUT);
    GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_ECUPC_nFAULT, GPIO_PINMUX_EXTINT);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_DATEL_nFAULT, GPIO_PINMODE_INPUT);
    GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_DATEL_nFAULT, GPIO_PINMUX_EXTINT);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_INV_F_nFAULT, GPIO_PINMODE_INPUT);
    GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_INV_F_nFAULT, GPIO_PINMUX_EXTINT);
    GPIO_SetPinMode(GPIO_PORTA, PDB_PIN_INV_A_nFAULT, GPIO_PINMODE_INPUT);  
    GPIO_SetPinMux(GPIO_PORTA, PDB_PIN_INV_A_nFAULT, GPIO_PINMUX_EXTINT);

    MCLK->APBAMASK.bit.EIC_ = 1;
    EIC->CONFIG[0].reg = EIC_CONFIG_SENSE3_FALL | EIC_CONFIG_SENSE4_FALL | EIC_CONFIG_SENSE5_FALL;  //Enable EXTINT[3], EXTINT[4] e EXTINT[5] on falling edge detection
	EIC->CONFIG[1].reg = EIC_CONFIG_SENSE5_BOTH | EIC_CONFIG_SENSE7_BOTH;	//Enable EXTINT[13] e EXTINT[15] on falling edge detection
	EIC->ASYNCH.reg = _BV(3) | _BV(4) | _BV(5) | _BV(13) | _BV(15);
	EIC->CTRLA.bit.ENABLE = 1;
	while (EIC->SYNCBUSY.bit.ENABLE){};
    EIC->INTENSET.reg = EIC_INTENSET_EXTINT(_BV(3) | _BV(4) | _BV(5) | _BV(13) | _BV(15));
	NVIC_EnableIRQ(EIC_IRQn);


    
    /*EXTINT_Initialize(SYSTEM_CLOCK_SOURCE_1MHZ_GCLK1);  
    
	EXTINT_ConfigureChannel(3, EXTINT_MODE_FALLING, TRUE, TRUE);
	EXTINT_ConfigureChannel(4, EXTINT_MODE_FALLING, TRUE, TRUE);
	EXTINT_ConfigureChannel(5, EXTINT_MODE_FALLING, TRUE, TRUE);
    EXTINT_ConfigureChannel(13, EXTINT_MODE_FALLING, TRUE, TRUE);
    EXTINT_ConfigureChannel(15, EXTINT_MODE_FALLING, TRUE, TRUE);*/

}

void PdbPwrMon_Task(){

    PdbPwrMon_FaultControl();
    PdbPwrMon_ADC();

    //Ext Sel
    if(PdbPwrMonData.ExtStatus != PDB_EXT_NOT_AV){
        if(GPIO_GetPinState(GPIO_PORTB, PDB_PIN_EXT_SEL)){
            PdbPwrMonData.ExtStatus = PDB_EXT_AV_SEL;
        }else{
            PdbPwrMonData.ExtStatus = PDB_EXT_AV;
        }
    }

    //Leds						
    if(PdbPwrMonData.ExtStatus == PDB_EXT_AV_SEL){
        if(SYSTEM_GetSysTickCount() - lastExtLedBlinkTime >= PARAM_EXT_SEL_BLINK_PERIOD){ 
            lastExtLedBlinkTime = SYSTEM_GetSysTickCount();
            GPIO_TogglePin(GPIO_PORTB, PDB_PIN_EXT_LED);					
        }
    }else if(PdbPwrMonData.ExtStatus == PDB_EXT_AV){
        GPIO_SetPinHigh(GPIO_PORTB, PDB_PIN_EXT_LED);
    }else if(PdbPwrMonData.ExtStatus == PDB_EXT_NOT_AV){
        GPIO_SetPinLow(GPIO_PORTB, PDB_PIN_EXT_LED);				
    }
    //TODO: Led2
}

void PdbPwrMon_ADC(){
    //TODO: configurare diversi VREF
    if(SYSTEM_GetSysTickCount() - lastAcqTime >= PARAM_ACQ_TIME){
        lastAcqTime = SYSTEM_GetSysTickCount();

        IVREF_Setup(IVREF_MODE_1V024);
	    ADC_Initialize(ADC0, ADC_VREF_INTERNAL, ADC_SPEED_G2(1));

        ADC_SetSingleEndedChannel(ADC0, 7); 
        ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
        while(!ADC_IsResultReady(ADC0));
        PdbPwrMonData.Imon12V[0] = PDB_ISCALE(ADC_GetResult(ADC0));// SAFAM Imon

        ADC_SetSingleEndedChannel(ADC0, 8); 
        ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
        while(!ADC_IsResultReady(ADC0));
        PdbPwrMonData.Imon12V[1] = PDB_ISCALE(ADC_GetResult(ADC0));// ECUPC Imon

        ADC_SetSingleEndedChannel(ADC0, 9); 
        ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
        while(!ADC_IsResultReady(ADC0));
        PdbPwrMonData.Imon12V[2] = PDB_ISCALE(ADC_GetResult(ADC0));// DATEL Imon

        ADC_Initialize(ADC0, ADC_VREF_VDD, ADC_SPEED_G2(1));

        ADC_SetSingleEndedChannel(ADC0, 6);
        ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
        while(!ADC_IsResultReady(ADC0));
        PdbPwrMonData.Tmon12V = PDB_TSCALE(ADC_GetResult(ADC0));// 12V Tmon

        IVREF_Setup(IVREF_MODE_2V048);
	    ADC_Initialize(ADC0, ADC_VREF_INTERNAL, ADC_SPEED_G2(1));

        ADC_SetSingleEndedChannel(ADC0, 1);
        ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
        while(!ADC_IsResultReady(ADC0));
        PdbPwrMonData.ImonInv[0] = PDB_ISCALE(ADC_GetResult(ADC0));// INV_F Imon

        ADC_SetSingleEndedChannel(ADC0, 4); 
        ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
        while(!ADC_IsResultReady(ADC0));
        PdbPwrMonData.ImonInv[1] = PDB_ISCALE(ADC_GetResult(ADC0));// INV_A Imon


        ADC_SetSingleEndedChannel(ADC0, 5);
        ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
        while(!ADC_IsResultReady(ADC0));
        PdbPwrMonData.Vmon[0] = PDB_12V_VSCALE(ADC_GetResult(ADC0));// 12V Vmon

        ADC_SetSingleEndedChannel(ADC0, 0); 
        ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
        while(!ADC_IsResultReady(ADC0));
        PdbPwrMonData.Vmon[1] = PDB_LVS_VSCALE(ADC_GetResult(ADC0));// LVS Vmon        

        ADC_SetSingleEndedChannel(ADC0, 2); 
        ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
        while(!ADC_IsResultReady(ADC0));
        PdbPwrMonData.Vmon[2] = PDB_LVS_VSCALE(ADC_GetResult(ADC0));// EXT Vmon
        if(PdbPwrMonData.Vmon[2] >= PARAM_EXT_AVAILABLE_MILLIVOLTS) 
            PdbPwrMonData.ExtStatus = PDB_EXT_AV;
        else{
            PdbPwrMonData.ExtStatus = PDB_EXT_NOT_AV;
            //errore se EXT_SEL?
        }

        CommSystem_SendData();
    }
}

PDB_PWRMON_DATA* PdbPwrMon_GetData(){
    return &PdbPwrMonData;
}

void PdbPwrMon_eFuseControl(BYTE eFuseControl){
    if((eFuseControl & PDB_eFUSE_SAFAM_MASK) != 0){
        GPIO_SetPinHigh(GPIO_PORTA, PDB_PIN_SAFAM_PEN);
    }else{
        GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_SAFAM_PEN);
    }

    if((eFuseControl & PDB_eFUSE_ECUPC_MASK) != 0){
        GPIO_SetPinHigh(GPIO_PORTA, PDB_PIN_ECUPC_PEN);
    }else{
        GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_ECUPC_PEN);
    }

    if((eFuseControl & PDB_eFUSE_DATEL_MASK) != 0){
        GPIO_SetPinHigh(GPIO_PORTA, PDB_PIN_DATEL_PEN);
    }else{
        GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_DATEL_PEN);
    }

    if((eFuseControl & PDB_eFUSE_INV_F_MASK) != 0){
        GPIO_SetPinHigh(GPIO_PORTA, PDB_PIN_INV_F_PEN);
    }else{
        GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_INV_F_PEN);
    }

    if((eFuseControl & PDB_eFUSE_INV_A_MASK) != 0){
        GPIO_SetPinHigh(GPIO_PORTA, PDB_PIN_INV_A_PEN);
    }else{
        GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_INV_A_PEN);
    }
    
    PdbPwrMonData.eFuseStatus = eFuseControl & PDB_eFUSE_MASK;
    //TODO: se voglio abilitare fusibile guasto?
}

void PdbPwrMon_FaultControl(){
    /*if(safamFaultTime > 0 && SYSTEM_GetSysTickCount() - safamFaultTime >= PARAM_eFUSE_BACKOFF){
        if(++safamFaults <= PARAM_eFUSE_FAULT_RETRY){
            GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_DATEL_PEN);
            GPIO_SetPinHigh(GPIO_PORTA, PDB_PIN_SAFAM_PEN);
            PdbPwrMonData.eFuseStatus |= PDB_eFUSE_SAFAM_MASK;
            safamFaultTime = 0;
        }else{
            GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_SAFAM_PEN);
            PdbPwrMonData.eFuseStatus &= ~PDB_eFUSE_SAFAM_MASK;
        }
    }
    if(ecupcFaultTime > 0 && SYSTEM_GetSysTickCount() - ecupcFaultTime >= PARAM_eFUSE_BACKOFF){
        if(++ecupcFaults <= PARAM_eFUSE_FAULT_RETRY){
            GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_DATEL_PEN);
            GPIO_SetPinHigh(GPIO_PORTA, PDB_PIN_ECUPC_PEN);
            PdbPwrMonData.eFuseStatus |= PDB_eFUSE_ECUPC_MASK;
            ecupcFaultTime = 0;
        }else{
            GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_ECUPC_PEN);
            PdbPwrMonData.eFuseStatus &= ~PDB_eFUSE_ECUPC_MASK;
        }
    }
    if(datelFaultTime > 0 && SYSTEM_GetSysTickCount() - datelFaultTime >= PARAM_eFUSE_BACKOFF){
        if(++datelFaults <= PARAM_eFUSE_FAULT_RETRY){
            GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_DATEL_PEN);
            GPIO_SetPinHigh(GPIO_PORTA, PDB_PIN_DATEL_PEN);
            PdbPwrMonData.eFuseStatus |= PDB_eFUSE_DATEL_MASK;
            datelFaultTime = 0;
        }else{
            GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_DATEL_PEN);
            PdbPwrMonData.eFuseStatus &= ~PDB_eFUSE_DATEL_MASK;
        }
    }*/
    if(invfFaultTime > 0 && SYSTEM_GetSysTickCount() - invfFaultTime >= PARAM_eFUSE_BACKOFF){
        CommSystem_SendFault(PDB_eFUSE_INV_F_MASK & 0xFF);
        if(invfFaults <= PARAM_eFUSE_FAULT_RETRY){
            GPIO_SetPinHigh(GPIO_PORTA, PDB_PIN_INV_F_PEN);
            PdbPwrMonData.eFuseStatus |= PDB_eFUSE_INV_F_MASK;
            invfFaultTime = 0;
        }else{
            GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_INV_F_PEN);
            PdbPwrMonData.eFuseStatus &= ~PDB_eFUSE_INV_F_MASK;
            invfFaultTime = 0;
        }
    }
    if(invaFaultTime > 0 && SYSTEM_GetSysTickCount() - invaFaultTime >= PARAM_eFUSE_BACKOFF){
        if(invaFaults <= PARAM_eFUSE_FAULT_RETRY){
            GPIO_SetPinHigh(GPIO_PORTA, PDB_PIN_INV_A_PEN);
            PdbPwrMonData.eFuseStatus |= PDB_eFUSE_INV_A_MASK;
            invaFaultTime = 0;
        }else{
            GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_INV_A_PEN);
            PdbPwrMonData.eFuseStatus &= ~PDB_eFUSE_INV_A_MASK;
        }
    }
   //TODO: dopo disabilitazione fusibile mettere [fuse]Faults = 0? Ora se riattivato dopo terzo guasto verrebbe spento subito
}

void EIC_Handler(void){
    
    DWORD intstate = EIC->INTFLAG.reg;
	if (intstate & _BV(5)){	//SAFAM nFault
		safamFaultTime = SYSTEM_GetSysTickCount();
        safamFaults++;
        CommSystem_SendFault(PDB_eFUSE_SAFAM_MASK & 0xFF);
	}
	if (intstate & _BV(13)){	//ECUPC nFault
		ecupcFaultTime = SYSTEM_GetSysTickCount();
        ecupcFaults++;
        CommSystem_SendFault(PDB_eFUSE_ECUPC_MASK & 0xFF);
	}
	if (intstate & _BV(15)){	//DATEL nFault
		datelFaultTime = SYSTEM_GetSysTickCount();
        datelFaults++;
        CommSystem_SendFault(PDB_eFUSE_DATEL_MASK & 0xFF);
	}
    if (intstate & _BV(3)){	//INV F nFault
		invfFaultTime = SYSTEM_GetSysTickCount();
        invfFaults++;
        GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_INV_F_PEN);
        
	}
    if (intstate & _BV(4)){	//INV A nFault
		invaFaultTime = SYSTEM_GetSysTickCount();//SYSTEM_GetSysTickCount();
        invaFaults = invaFaults +1;
        GPIO_SetPinLow(GPIO_PORTA, PDB_PIN_INV_A_PEN);
        CommSystem_SendFault(PDB_eFUSE_INV_A_MASK & 0xFF);
	}
    EIC->INTFLAG.reg = intstate;
}