/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _SERCOM_DRIVER_H
#define _SERCOM_DRIVER_H

#include "common.h"

typedef struct uart_peripheral {
	Sercom* Sercom;
	DWORD ICtrlA;
	DWORD CurrentBaudRate;
   FIFO_BUFFER* RxBuffer;
   FIFO_BUFFER* TxBuffer;
   BYTE SercomInstance;
} UART_PERIPHERAL;

#define UART_MODE_NORMAL 0
#define UART_MODE_RS485 1
#define UART_MODE_FLOWCONTROL 2

BOOLEAN UART_SetMode(UART_PERIPHERAL* periph, DWORD baudrate, DWORD mode);
BOOLEAN UART_LinkBuffers(UART_PERIPHERAL* periph, FIFO_BUFFER* rx_buffer, FIFO_BUFFER* tx_buffer);
BOOLEAN UART_Initialize(UART_PERIPHERAL* periph, DWORD sercom_instance);
DWORD UART_TxLock(HANDLE periph);
void UART_TxUnlock(HANDLE periph, DWORD lock);
DWORD UART_RxLock(HANDLE periph);
void UART_RxUnlock(HANDLE periph, DWORD lock);

#define I2C_SPEED_NORMAL 100000
#define I2C_SPEED_FAST 400000

typedef struct i2c_peripheral {
	Sercom* Sercom;
   DWORD Speed;
   BYTE SercomInstance;
} I2C_PERIPHERAL;

BOOLEAN I2C_SetMode(I2C_PERIPHERAL* periph, DWORD speed);
BOOLEAN I2C_Initialize(I2C_PERIPHERAL* periph, DWORD sercom_instance);
BOOLEAN I2C_ReadRegisterBA(I2C_PERIPHERAL* periph, BYTE addr, BYTE reg, WORD len, BYTE *dest);
BOOLEAN I2C_WriteRegisterBA(I2C_PERIPHERAL* periph, BYTE addr, BYTE reg, WORD len, BYTE *data);
BOOLEAN I2C_ReadRegisterWA(I2C_PERIPHERAL* periph, BYTE addr, WORD reg, WORD len, BYTE *dest);
BOOLEAN I2C_WriteRegisterWA(I2C_PERIPHERAL* periph, BYTE addr, WORD reg, WORD len, BYTE *data);

BOOLEAN I2C_Write(I2C_PERIPHERAL* periph, BYTE addr, WORD len, BYTE *data, BOOLEAN noStop);
BOOLEAN I2C_Read(I2C_PERIPHERAL* periph, BYTE addr, WORD len, BYTE *dest);

#endif