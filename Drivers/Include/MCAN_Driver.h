/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MCAN_DRIVER_H
#define _MCAN_DRIVER_H

#include "samc21.h"
#include "common.h"
#include "General_Config.h"

typedef struct mcan_configuration {
	BYTE RxStandardFilters;
	BYTE RxExtendedFilters;
	BYTE RxDedicatedBuffers;
	BYTE RxFifo0Length;
	BYTE RxFifo1Length;
	BYTE TxDedicatedBuffers;
	BYTE TxFifoLenght;
	BYTE TxEventFifoLenght;
	DWORD BitTimings;
	BYTE GlobalFilter;
	BYTE StdInitialFiltersNum;
	BYTE ExtInitialFiltersNum;
	DWORD ExtendedIdAndMask;
	DWORD* StdInitialFilters;
	DWORD* ExtInitialFilters;
} MCAN_CONFIGURATION;

typedef struct mcan_memory {
	DWORD* pRamStandardFilters;
	DWORD* pRamExtendedFilters;
	DWORD* pRamRXFifo0;
	DWORD* pRamRXFifo1;
	DWORD* pRamRXDedicated;
	DWORD* pRamTXEvent;
	DWORD* pRamTXBuffers;
} _mcan_memory_internal;

typedef struct mcan_peripheral {
	Can* Peripheral;
	DWORD CurrentMode;
	MCAN_CONFIGURATION* Config;
	_mcan_memory_internal Memory;
} MCAN_PERIPHERAL;

typedef struct mcan_message {
	DWORD Id;
	BYTE Options; //EXTENDED_ID, REMOTE_FRAME
	BYTE Lenght;
	BYTE Data[8] __ALIGNED(4);
} MCAN_MESSAGE;

#define MCAN_MESSAGE_OPTION_EXTENDEDID (0x01)
#define MCAN_MESSAGE_OPTION_REMOTEFRAME (0x02)

#define MCAN_MODE_DISABLED 0
#define MCAN_MODE_CONFIGURE 1
#define MCAN_MODE_ENABLED 2
#define MCAN_MODE_MONITOR 3
#define MCAN_MODE_BUS_OFF 10

#define MCAN_GF_REMOTEFRAMES_ACCEPTALL 0
#define MCAN_GF_REMOTEFRAMES_REJECTSTD 2
#define MCAN_GF_REMOTEFRAMES_REJECTEXT 1
#define MCAN_GF_REMOTEFRAMES_REJECTALL 3

#define MCAN_GF_NONMATCHING_FIFO0 0
#define MCAN_GF_NONMATCHING_FIFO1 1
#define MCAN_GF_NONMATCHING_REJECT 2

#if(MCAN0_ENABLED)
extern MCAN_PERIPHERAL MCAN0;
#endif
#if(MCAN1_ENABLED)
extern MCAN_PERIPHERAL MCAN1;
#endif

void MCAN_Initialize(MCAN_PERIPHERAL* instance);
BOOLEAN MCAN_ConfigureBitTimings(MCAN_PERIPHERAL *instance, DWORD sjw, DWORD seg1, DWORD seg2, DWORD prescaler);
BOOLEAN MCAN_ConfigureGlobalFilter(MCAN_PERIPHERAL *instance, DWORD remote_frame, DWORD non_matching_std, DWORD non_matching_ext);
BOOLEAN MCAN_ConfigureExtendedIdGlobalMask(MCAN_PERIPHERAL *instance, DWORD ext_id_mask);
BOOLEAN MCAN_LoadStandardFilter(MCAN_PERIPHERAL *instance, DWORD filternum, DWORD filter);
BOOLEAN MCAN_LoadExtendedFilter(MCAN_PERIPHERAL *instance, DWORD filternum, DWORD* filter);

DWORD MCAN_GetCurrentMode(MCAN_PERIPHERAL *instance);
BOOLEAN MCAN_ChangeMode(MCAN_PERIPHERAL *instance, DWORD mode);

BOOLEAN MCAN_ReadFIFOMessage(MCAN_PERIPHERAL *instance, DWORD fifo, MCAN_MESSAGE *msg);
BOOLEAN MCAN_TransmitMessageFIFO(MCAN_PERIPHERAL *instance, MCAN_MESSAGE *msg);

#endif