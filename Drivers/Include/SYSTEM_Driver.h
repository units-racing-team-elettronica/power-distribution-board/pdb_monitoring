/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _SYSTEM_DRIVER_H
#define _SYSTEM_DRIVER_H

#include "common.h"
#include "samc21.h"

#define SYSTEM_OSCILLATOR_INTOSC 0
#define SYSTEM_OSCILLATOR_EXTOSC 1

#define SYSTEM_CLOCK_SOURCE_48MHZCPU_GCLK0 0x1
#define SYSTEM_CLOCK_SOURCE_1MHZ_GCLK1 0x2
#define SYSTEM_CLOCK_SOURCE_32MHZADC_GCLK2 0x4
#define SYSTEM_CLOCK_SOURCE_32kHZULP_GCLK7 0x8

void SYSTEM_InitializeClocks(DWORD oscillator);
BOOLEAN SYSTEM_IsClockSourceAvailable(DWORD source);

typedef struct critical_section {
   DWORD Mask;
} CRITICAL_SECTION;

__STATIC_FORCEINLINE void SYSTEM_EnterCriticalSection(CRITICAL_SECTION* cs) {
    cs->Mask = __get_PRIMASK();
    __disable_irq();
    __COMPILER_BARRIER();
}

__STATIC_FORCEINLINE void SYSTEM_LeaveCriticalSection(CRITICAL_SECTION* cs) {
   if(!cs->Mask)
      __enable_irq();
   __COMPILER_BARRIER();
}

void SYSTEM_InitalizeSysTick(DWORD freq);
DWORD SYSTEM_GetSysTickCount();
void SYSTEM_UpdateSysTickCount(DWORD count);
void SYSTEM_IncrementSysTickCount(DWORD increment);
BOOLEAN SYSTEM_RegisterSysTickCallback(void (*callback)(void));
BOOLEAN SYSTEM_UnregisterSysTickCallback(void (*callback)(void));
void SYSTEM_ClearSysTickCallbacks();


#endif