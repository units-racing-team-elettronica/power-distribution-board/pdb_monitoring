/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "GPIO_Driver.h"
#include "SYSTEM_Driver.h"

void GPIO_SetPinMode(DWORD port, DWORD pin, DWORD mode)
{
    if(mode == GPIO_PINMODE_INPUT) {
        PORT->Group[port].DIRCLR.reg = 1 << pin;
        PORT->Group[port].PINCFG[pin].reg = PORT_PINCFG_INEN;
    } else if (mode == GPIO_PINMODE_INPUT_PULLUP) {
        PORT->Group[port].DIRCLR.reg = 1 << pin;
        PORT->Group[port].OUTCLR.reg = 1 << pin;
        PORT->Group[port].PINCFG[pin].reg = PORT_PINCFG_INEN | PORT_PINCFG_PULLEN;
    } else if (mode == GPIO_PINMODE_INPUT_PULLDOWN) {
        PORT->Group[port].DIRCLR.reg = 1 << pin;
        PORT->Group[port].OUTSET.reg = 1 << pin;
        PORT->Group[port].PINCFG[pin].reg = PORT_PINCFG_INEN | PORT_PINCFG_PULLEN;
    } else if (mode == GPIO_PINMODE_OUTPUT) {
        PORT->Group[port].OUTCLR.reg = 1 << pin;
        PORT->Group[port].DIRSET.reg = 1 << pin;
        PORT->Group[port].PINCFG[pin].reg = PORT_PINCFG_INEN;
    } else if (mode == GPIO_PINMODE_ANALOG) {
        PORT->Group[port].DIRCLR.reg = 1 << pin;
        PORT->Group[port].PINCFG[pin].reg = 0;
    }
}

void GPIO_SetPinMux(DWORD port, DWORD pin, DWORD function)
{
    PORT->Group[port].PINCFG[pin].bit.PMUXEN = 0;
    if(function >= GPIO_PINMUX_DISABLE)
        return;
    PORT->Group[port].PMUX[pin / 2].reg &= ~(0x0F << ((pin % 2) * 4));
    PORT->Group[port].PMUX[pin / 2].reg |= (function & 0x0F) << ((pin % 2) * 4);
    PORT->Group[port].PINCFG[pin].bit.PMUXEN = 1;
}

//Funzioni candidate per la trasformazione in macro

BOOLEAN GPIO_GetPinState(DWORD port, DWORD pin) {
    if(PORT->Group[port].IN.reg & (1 << pin))
        return TRUE;
    else
        return FALSE;
}


BOOLEAN EXTINT_Initialize(DWORD clocksource)
 {
    if(!SYSTEM_IsClockSourceAvailable(clocksource))
        return FALSE;
    DWORD gclock_value = 1;
    if(clocksource == SYSTEM_CLOCK_SOURCE_48MHZCPU_GCLK0) {
        gclock_value = 0;
    } else if(clocksource == SYSTEM_CLOCK_SOURCE_1MHZ_GCLK1) {
        gclock_value = 1;
    } else if(clocksource == SYSTEM_CLOCK_SOURCE_32MHZADC_GCLK2) {
        gclock_value = 2;
    } else if(clocksource == SYSTEM_CLOCK_SOURCE_32kHZULP_GCLK7) {
        gclock_value = 7;
    } 
    MCLK->APBAMASK.bit.EIC_ = 1;
	GCLK->PCHCTRL[EIC_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | gclock_value;
	while(GCLK->PCHCTRL[EIC_GCLK_ID].bit.CHEN == 0){}
    NVIC_EnableIRQ(EIC_IRQn);
    return TRUE;
}

void EXTINT_ConfigureNMI(DWORD mode, BOOLEAN filter) 
{
    DWORD reg = EIC_NMICTRL_NMISENSE(mode);
    if(filter)
        reg |= EIC_NMICTRL_NMIFILTEN;
    EIC->NMICTRL.reg = reg;
}

void EXTINT_ConfigureChannel(DWORD channel, DWORD mode, BOOLEAN filter, BOOLEAN enable) 
{
    DWORD reg = EIC_CONFIG_SENSE0(mode);
    if(filter)
        reg |= EIC_CONFIG_FILTEN0;
    while(EIC->SYNCBUSY.bit.ENABLE){}
    if(EIC->CTRLA.bit.ENABLE) {
        EIC->CTRLA.bit.ENABLE = 0;
        while(EIC->SYNCBUSY.bit.ENABLE){}
    }
    EIC->CONFIG[channel / 8].reg &= 0x0F << ((channel % 8) * 4);
    EIC->CONFIG[channel / 8].reg |= (reg & 0x0F) << ((channel % 8) * 4);
    EIC->CTRLA.bit.ENABLE = 1;
    while(EIC->SYNCBUSY.bit.ENABLE){}
    if(enable)
        EIC->INTENSET.reg = 1 << channel;
    else
        EIC->INTENCLR.reg = 1 << channel;
}
