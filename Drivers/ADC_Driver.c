/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ADC_Driver.h"
#include "SYSTEM_Driver.h"

void IVREF_Setup(DWORD mode) 
{
    if(mode == IVREF_MODE_OFF)
        SUPC->VREF.reg = 0;
    else
        SUPC->VREF.reg = mode | SUPC_VREF_VREFOE;
}

void ADC_Initialize(ADC_PERIPHERAL *instance, BYTE vref, BYTE speed)
{
    DWORD gclock_value = SYSTEM_IsClockSourceAvailable(SYSTEM_CLOCK_SOURCE_32MHZADC_GCLK2) ? 2 : 0;
    if((speed & 0xF0) == 0x10)
        gclock_value = 0;
    else if((speed & 0xF0) == 0x30)
        gclock_value = 2;
    if(instance == ADC0) {
		MCLK->APBCMASK.bit.ADC0_ = 1;
	    GCLK->PCHCTRL[ADC0_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | gclock_value;
	    while(GCLK->PCHCTRL[ADC0_GCLK_ID].bit.CHEN == 0){} 
	} else if(instance == ADC1) {
		MCLK->APBCMASK.bit.ADC1_ = 1;
	    GCLK->PCHCTRL[ADC1_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | gclock_value;
	    while(GCLK->PCHCTRL[ADC1_GCLK_ID].bit.CHEN == 0){}
	}

    instance->CTRLA.bit.SWRST = 1;
    while(instance->SYNCBUSY.bit.SWRST) {};
    if(speed == 0) {
        if(gclock_value == 0)
            instance->CTRLB.reg = ADC_CTRLB_PRESCALER_DIV4; //12Mhz clock from main 48MHz
        else
            instance->CTRLB.reg = ADC_CTRLB_PRESCALER_DIV2; //16Mhz clock from adc/gclock2 32MHz
    } else {
        instance->CTRLB.reg = speed & 0x0F;
    }
    
	instance->SAMPCTRL.bit.OFFCOMP = 1;
	instance->CTRLC.bit.R2R = 1;
	instance->INPUTCTRL.reg = ADC_INPUTCTRL_MUXNEG(0x18);

    DWORD softcal = *((PDWORD)NVMCTRL_OTP5); //Calibration constants MUST BE LOADED FROM NVM, badly documented (https://www.avrfreaks.net/forum/adc-calib-register-saml22)
    if(instance == ADC0) 
        ADC0->CALIB.reg = (DWORD)(ADC_CALIB_BIASREFBUF(softcal & 0x003) | ADC_CALIB_BIASCOMP((softcal & 0x0038) >> 3));
    else if(instance == ADC1)
        ADC1->CALIB.reg = (DWORD)(ADC_CALIB_BIASREFBUF((softcal & 0x01C0) >> 6) | ADC_CALIB_BIASCOMP((softcal & 0x0E00) >> 9));

    instance->REFCTRL.reg = vref | ADC_REFCTRL_REFCOMP; //TODO: Add option to configure VREF BUFFER COMPENSATION (REFCOMP)

    instance->CTRLA.bit.ENABLE = 1;
    while(instance->SYNCBUSY.bit.ENABLE) {}
    //Do a conversion discarding the value to initialize the reference
    instance->SWTRIG.bit.START = 1;
    while(instance->INTFLAG.bit.RESRDY == 0) {}
    instance->INTFLAG.reg = ADC_INTFLAG_RESRDY; //Clear Result Ready Flag
}

void ADC_ChangeReference(ADC_PERIPHERAL *instance, BYTE vref)
{
    instance->CTRLA.bit.ENABLE = 0;
    while(instance->SYNCBUSY.bit.ENABLE) {}
    instance->REFCTRL.reg = vref;
    instance->CTRLA.bit.ENABLE = 1;
    while(instance->SYNCBUSY.bit.ENABLE) {}
    //Do a conversion discarding the value to initialize the reference
    instance->SWTRIG.bit.START = 1;
    while(instance->INTFLAG.bit.RESRDY == 0) {}
    instance->INTFLAG.reg = ADC_INTFLAG_RESRDY; //Clear Result Ready Flag
}

void ADC_SetEvents(ADC_PERIPHERAL *instance, BYTE events)
{
    instance->CTRLA.bit.ENABLE = 0;
    while(instance->SYNCBUSY.bit.ENABLE) {}
    instance->EVCTRL.reg = events;
    instance->CTRLA.bit.ENABLE = 1;
    while(instance->SYNCBUSY.bit.ENABLE) {}
}

void ADC_SetSingleEndedChannel(ADC_PERIPHERAL *instance, DWORD channel)
{
    instance->CTRLC.bit.DIFFMODE = 0;
    instance->INPUTCTRL.reg = ADC_INPUTCTRL_MUXNEG(0x18) | ADC_INPUTCTRL_MUXPOS(channel);
    while(instance->SYNCBUSY.bit.INPUTCTRL) {}
}

void ADC_SetDifferentialChannel(ADC_PERIPHERAL *instance, DWORD poschannel, DWORD negchannel)
{
    instance->CTRLC.bit.DIFFMODE = 1;
    instance->INPUTCTRL.reg = ADC_INPUTCTRL_MUXNEG(negchannel) | ADC_INPUTCTRL_MUXPOS(poschannel);
    while(instance->SYNCBUSY.bit.INPUTCTRL) {}
}

void ADC_SetSequence(ADC_PERIPHERAL *instance, DWORD sequence)
{
    instance->SEQCTRL.reg = sequence;
}

void ADC_SetAveraging(ADC_PERIPHERAL *instance, DWORD mode, DWORD samples)
{
    if(mode == ADC_AVERAGING_DISABLE || samples == 0) {
        instance->AVGCTRL.reg = 0;
        instance->CTRLC.bit.RESSEL = 0;
    } else if(mode == ADC_AVERAGING_OVERSAMPLE) {
        switch(samples) {
            case ADC_OVERSAMPLE_13BITS: instance->AVGCTRL.reg = ADC_AVGCTRL_SAMPLENUM(ADC_OVERSAMPLE_13BITS) | ADC_AVGCTRL_ADJRES(0x01); break;
            case ADC_OVERSAMPLE_14BITS: instance->AVGCTRL.reg = ADC_AVGCTRL_SAMPLENUM(ADC_OVERSAMPLE_14BITS) | ADC_AVGCTRL_ADJRES(0x02); break;
            case ADC_OVERSAMPLE_15BITS: instance->AVGCTRL.reg = ADC_AVGCTRL_SAMPLENUM(ADC_OVERSAMPLE_15BITS) | ADC_AVGCTRL_ADJRES(0x01); break;
            case ADC_OVERSAMPLE_16BITS: instance->AVGCTRL.reg = ADC_AVGCTRL_SAMPLENUM(ADC_OVERSAMPLE_16BITS); break;
        }
        instance->CTRLC.bit.RESSEL = 1;
    } else if(mode == ADC_AVERAGING_ACCUMULATE) {
        instance->AVGCTRL.reg = ADC_AVGCTRL_SAMPLENUM(samples);
        instance->CTRLC.bit.RESSEL = 1;
    } else if(mode == ADC_AVERAGING_AVERAGE) {
        DWORD divisor = samples;
        if(divisor > 0x04)
            divisor = 0x04;
        instance->AVGCTRL.reg = ADC_AVGCTRL_SAMPLENUM(samples) | ADC_AVGCTRL_ADJRES(divisor);
        instance->CTRLC.bit.RESSEL = 1;
    }
    while(instance->SYNCBUSY.reg & (ADC_SYNCBUSY_CTRLC | ADC_SYNCBUSY_AVGCTRL)) {}
}

void ADC_SetWindowMonitor(ADC_PERIPHERAL *instance, DWORD mode, WORD lvalue, WORD uvalue)
{
    instance->CTRLC.bit.WINMODE = mode;
    instance->WINLT.reg = lvalue;
    instance->WINUT.reg = uvalue;
    while(instance->SYNCBUSY.reg & (ADC_SYNCBUSY_CTRLC | ADC_SYNCBUSY_WINLT | ADC_SYNCBUSY_WINUT)) {}
}

void ADC_StartConversion(ADC_PERIPHERAL *instance, DWORD mode) 
{
    instance->CTRLC.bit.FREERUN = (mode & 0x01);
    while(instance->SYNCBUSY.reg & (ADC_SYNCBUSY_CTRLC)) {}
    instance->SWTRIG.bit.START = 1;
}

void ADC_StopConversion(ADC_PERIPHERAL *instance) 
{
    instance->CTRLC.bit.FREERUN = 0;
    instance->SWTRIG.bit.FLUSH = 1;
    while(instance->SYNCBUSY.reg & (ADC_SYNCBUSY_CTRLC)) {}
}

