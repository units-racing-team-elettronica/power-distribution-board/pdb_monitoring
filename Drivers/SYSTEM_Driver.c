/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SYSTEM_Driver.h"
#include "General_Config.h"

static DWORD SystemClockStatus = SYSTEM_CLOCK_SOURCE_48MHZCPU_GCLK0;
static volatile DWORD SysTickCounter = 0;
void (*SysTickCallbacks[SYSTEM_MAX_SYSTICK_CALLBACKS])(void);

void SYSTEM_InitializeClocks(DWORD oscillator)
{
    if(oscillator == SYSTEM_OSCILLATOR_EXTOSC) {
        /*WARNING: CFD is not usable: enabling CFD causes immediate clock failure detetction and switches to UNDIVIDED 48MHz clock from OSC48M: 
        STATUS.CLKFAIL = 0 (no fail detected) but STATUS.CLKSW = 1 and INTFLAG.CLKFAIL = 1 */
        //For usage with the CFD (safe clock source)
        /*OSCCTRL->OSC48MDIV.reg = OSCCTRL_OSC48MDIV_DIV(2);
        while (OSCCTRL->OSC48MSYNCBUSY.reg);*/

        OSCCTRL->XOSCCTRL.reg = OSCCTRL_XOSCCTRL_STARTUP(0x0A) | OSCCTRL_XOSCCTRL_AMPGC | OSCCTRL_XOSCCTRL_GAIN(0x3) | OSCCTRL_XOSCCTRL_XTALEN  | OSCCTRL_XOSCCTRL_ENABLE | OSCCTRL_XOSCCTRL_ONDEMAND; // OSCCTRL_XOSCCTRL_CFDEN
        GCLK->GENCTRL[1].reg = GCLK_GENCTRL_DIV(16) | GCLK_GENCTRL_GENEN | GCLK_GENCTRL_SRC_XOSC | GCLK_GENCTRL_OE;
        //OSCCTRL->XOSCCTRL.bit.CFDEN = 1;
        
        OSCCTRL->DPLLRATIO.reg = OSCCTRL_DPLLRATIO_LDR(95);
        OSCCTRL->DPLLCTRLB.reg = OSCCTRL_DPLLCTRLB_REFCLK(1) | OSCCTRL_DPLLCTRLB_DIV(7); //REFCLK 1 => XOSC
        OSCCTRL->DPLLPRESC.reg = 0;
        OSCCTRL->DPLLCTRLA.reg = OSCCTRL_DPLLCTRLA_ENABLE | OSCCTRL_DPLLCTRLA_ONDEMAND;
        while((OSCCTRL->DPLLSYNCBUSY.bit.ENABLE));

        GCLK->GENCTRL[0].reg = GCLK_GENCTRL_DIV(2) | GCLK_GENCTRL_GENEN | GCLK_GENCTRL_SRC_DPLL96M | GCLK_GENCTRL_OE;
    } else {
        GCLK->GENCTRL[1].reg = GCLK_GENCTRL_DIV(48) | GCLK_GENCTRL_GENEN | GCLK_GENCTRL_SRC_OSC48M;
        GCLK->PCHCTRL[OSCCTRL_GCLK_ID_FDPLL].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN_GCLK1;
		while(GCLK->PCHCTRL[OSCCTRL_GCLK_ID_FDPLL].bit.CHEN == 0){} 
        
        OSCCTRL->DPLLRATIO.reg = OSCCTRL_DPLLRATIO_LDR(95);
        OSCCTRL->DPLLCTRLB.reg = OSCCTRL_DPLLCTRLB_REFCLK(2); //REFCLK 1 => GCLK
        OSCCTRL->DPLLPRESC.reg = 0;
        OSCCTRL->DPLLCTRLA.reg = OSCCTRL_DPLLCTRLA_ENABLE | OSCCTRL_DPLLCTRLA_ONDEMAND;
        while((OSCCTRL->DPLLSYNCBUSY.bit.ENABLE));
    }
	GCLK->GENCTRL[2].reg = GCLK_GENCTRL_DIV(3) | GCLK_GENCTRL_GENEN | GCLK_GENCTRL_SRC_DPLL96M;
	GCLK->GENCTRL[7].reg = GCLK_GENCTRL_DIV(0) | GCLK_GENCTRL_GENEN | GCLK_GENCTRL_SRC_OSCULP32K;
    SystemClockStatus = 0x0F;
}

BOOLEAN SYSTEM_IsClockSourceAvailable(DWORD source) {
    return (SystemClockStatus & source) != 0;
}

void SYSTEM_InitalizeSysTick(DWORD freq) {
    SysTick_Config(SystemCoreClock/freq);
}

DWORD SYSTEM_GetSysTickCount() {
    return SysTickCounter;
}

//Attenzione: Questa funzione non è disponibile se si utilizza un RTOS
void SYSTEM_UpdateSysTickCount(DWORD count) {
    SysTickCounter = count;
}

//Attenzione: Questa funzione non è disponibile se si utilizza un RTOS
void SYSTEM_IncrementSysTickCount(DWORD increment) {
    SysTickCounter += increment;
}

BOOLEAN SYSTEM_RegisterSysTickCallback(void (*callback)(void)) {
    for(DWORD i = 0; i < SYSTEM_MAX_SYSTICK_CALLBACKS; i++) {
        if(SysTickCallbacks[i] == NULL) {
            SysTickCallbacks[i] = callback;
            return TRUE;
        }
    }
    return FALSE;
}

BOOLEAN SYSTEM_UnregisterSysTickCallback(void (*callback)(void)) {
    for(DWORD i = 0; i < SYSTEM_MAX_SYSTICK_CALLBACKS; i++) {
        if(SysTickCallbacks[i] == callback) {
            SysTickCallbacks[i] = NULL;
            return TRUE;
        }
    }
    return FALSE;
}

void SYSTEM_ClearSysTickCallbacks() {
    for(DWORD i = 0; i < SYSTEM_MAX_SYSTICK_CALLBACKS; i++) {
            SysTickCallbacks[i] = NULL;
    }
}

void SysTick_Handler() {
    SysTickCounter++;
    //Execute SysTick Callbacks
    for(DWORD i = 0; i < SYSTEM_MAX_SYSTICK_CALLBACKS; i++) {
        if(SysTickCallbacks[i] != NULL) {
            SysTickCallbacks[i]();
        }
    }
}
