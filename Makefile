##############################################################################
OUTPUT = Output
BIN = PDB_Monitoring

##############################################################################
.PHONY: all directory clean size map rebuild

CC = arm-none-eabi-gcc
OBJCOPY = arm-none-eabi-objcopy
OBJDUMP = arm-none-eabi-objdump
SIZE = arm-none-eabi-size

CFLAGS += -W -Wall --std=gnu99 -g3 -Og
CFLAGS += -fno-diagnostics-show-caret
CFLAGS += -fdata-sections -ffunction-sections
CFLAGS += -funsigned-char -funsigned-bitfields -fwrapv -fno-strict-aliasing
CFLAGS += -mcpu=cortex-m0plus -mthumb
CFLAGS += -MD -MP -MT $(OUTPUT)/$(*F).o -MF $(OUTPUT)/$(@F).d

LDFLAGS += -mcpu=cortex-m0plus -mthumb
LDFLAGS += --specs=nano.specs
LDFLAGS += -Wl,--gc-sections
LDFLAGS += -Wl,--script=./samc21.ld
LDFLAGS += -Wl,--cref -Xlinker -Map=$(OUTPUT)/$(BIN).map

INCLUDES += \
	-I./CMSIS/Core \
	-I./CMSIS/Device/Include \
	-I./Common/Include \
	-I./Drivers/Include \
	-I./Config \
	-I./Program/Include

SRCS += \
	./CMSIS/Device/startup_samc21.c \
	./CMSIS/Device/system_samc21.c \
	./Common/fifobuffer.c \
	./Common/printf.c \
	./Drivers/ADC_Driver.c \
	./Drivers/GPIO_Driver.c \
	./Drivers/MCAN_Driver.c \
	./Drivers/SERCOM_Driver.c \
	./Drivers/SYSTEM_Driver.c \
	./Config/MCAN_Config.c \
	./main.c \
	./Program/CommSystem.c \
	./Program/PdbPowerMonitor.c

DEFINES += \
	-D__SAMC21G18A__

CFLAGS += $(INCLUDES) $(DEFINES)

OBJS = $(addprefix $(OUTPUT)/, $(notdir %/$(subst .c,.o, $(SRCS))))

rebuild: clean all

all: directory $(OUTPUT)/$(BIN).elf $(OUTPUT)/$(BIN).hex $(OUTPUT)/$(BIN).bin size

$(OUTPUT)/$(BIN).elf: $(OBJS)
	@echo LD $@
	@$(CC) $(LDFLAGS) $(OBJS) $(LIBS) -o $@

$(OUTPUT)/$(BIN).hex: $(OUTPUT)/$(BIN).elf
	@echo OBJCOPY $@
	@$(OBJCOPY) -O ihex $^ $@

$(OUTPUT)/$(BIN).bin: $(OUTPUT)/$(BIN).elf
	@echo OBJCOPY $@
	@$(OBJCOPY) -O binary $^ $@

%.o:
	@echo CC $@
	@$(CC) $(CFLAGS) $(filter %/$(subst .o,.c,$(notdir $@)), $(SRCS)) -c -o $@

directory:
	@cmd /c "IF NOT EXIST $(OUTPUT) MKDIR $(OUTPUT)"

size: $(OUTPUT)/$(BIN).elf
	@echo Single compliation unit size:
	@$(SIZE) -t $(OUTPUT)/*.o
	@echo Total compiled image size:
	@$(SIZE) $^

clean:
	@echo clean
	@cmd /c "RMDIR /S /Q $(OUTPUT)"

-include $(wildcard $(OUTPUT)/*.d)
