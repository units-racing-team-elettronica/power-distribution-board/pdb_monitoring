#include "samc21.h"

// Initial system clock frequency. The 48Mhz Internal RC Oscillator (OSC48M) provides the source for the main clock at chip startup (/12).
#define SYSTEM_BOOT_CLOCK (4000000) //OSC48M (/12)
#define INTERNAL_OSCILLATOR_CLOCK (48000000) //OSC48M (/1)

//Mark SystemCoreClock PERSISTENT as it is used before C library initalization.
#if defined ( __CC_ARM )
  uint32_t SystemCoreClock __attribute__((section("persistent"),zero_init)); 
#elif defined ( __GNUC__ )
  uint32_t SystemCoreClock  __attribute__((section(".persistent"))); 
#endif

/**
 * Initialize the system
 *
 * @brief  Setup the microcontroller system.
 *         Initialize the System and update the SystemCoreClock variable.
 */
//WARNING: This fuctions runs BEFORE C library initalization: DO NOT USE .data (RW), .bss (ZI) VARIABILES.
void SystemInit(void)
{
	SystemCoreClock = SYSTEM_BOOT_CLOCK;
	NVMCTRL->CTRLB.reg = NVMCTRL_CTRLB_RWS(2) | NVMCTRL_CTRLB_MANW; 	// Set flash wait states to 2 for 48 MHz operation
	OSCCTRL->OSC48MDIV.reg = OSCCTRL_OSC48MDIV_DIV(0); 					// Switch to 48MHz clock, internal oscillator (disable prescaler)
	while (OSCCTRL->OSC48MSYNCBUSY.reg);
	SystemCoreClock = INTERNAL_OSCILLATOR_CLOCK;						//CPU Clock is now running from OSC48M (/1) --> GCLK_GEN0 (/1) --> MCLK (CPU)
	//Other clock configuration (external oscillator, RTC...) will be done by the SYSTEM Driver module, not in CMSIS SystemInit
}

/**
 * Update SystemCoreClock variable
 *
 * @brief  Updates the SystemCoreClock with current core Clock
 *         retrieved from cpu registers.
 */
void SystemCoreClockUpdate(void)
{
	// Not implemented
	return;
}
